const express = require('express')
const app = express()
const port = 3000

app.set('view engine', 'ejs');
app.get('/', (req, res) => {
  const text = "EID";
  const text2 = "MUBARAK";
  const date = "2021";
  const greet = "Minal Aidin Wal Faidzin";
  res.render('lebaran', {text, text2, date, greet})
})

app.get('/kue', (req, res) => {
    res.send([
      {
        id: 1,
        namaKue: "Nastar",
        deskripsi: "kue dengan selai nanas",
      },

      {
        id: 2,
        namaKue: "Putri Salju",
        deskripsi: "kue dengan tepung gula",
      },

      {
        id: 3,
        namaKue: "Kastengel",
        deskripsi: "kue dengan parutan keju",
      }

    ])
  })

  app.get('/makanan', (req, res) => {
    res.send([
      {
        id: 1,
        namaMakanan: "Ketupat",
        deskripsi: "Kupat Sayur",
      },

      {
        id: 2,
        namaMakanan: "Opor",
        deskripsi: "Opor ayam",
      },

      {
        id: 3,
        namaMakanan: "Rendang",
        deskripsi: "gulai rendang",
      },
      {
        id: 4,
        namaMakanan: "Semur",
        deskripsi: "Semur Daging",
      }

    ])
  })
app.listen(port, () => {
  console.log(`server udah jalan bos! coba cek localhost:${port}`)
})